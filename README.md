# dockerizing-djang-nginx-gunicorn-mysql
Dockerizing Django with Mysql, Gunicorn, and Nginx

### Dependencies
1. Django v2.1
2. Python v3.7

Based on this repo [testdrivenio/django-on-docker](https://github.com/testdrivenio/django-on-docker) (Dockerizing Django with Postgres, Gunicorn, and Nginx)

### How to use?
`$ docker-compose up -d --build`
